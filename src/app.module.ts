import { Module, Controller, Injectable, Scope, Inject, Get } from '@nestjs/common';
import { REQUEST } from '@nestjs/core'

@Injectable({ scope: Scope.REQUEST, durable: false })
class AppService {
  constructor(@Inject(REQUEST) private readonly req: any) {
    console.log('Is req fasly?', !!req === false) // true but should be false
  }

  isReqDefined() {
    return typeof this.req !== 'undefined' // false but should be true
  }
}

@Controller() // request-scoped but not durable, due to its dependency
class AppController {
  constructor(private readonly appService: AppService) {
    console.log('AppController is initialized')
  }

  @Get()
  getHello() {
    return {
      is_req_defined: this.appService.isReqDefined(),
    }
  }
}


@Module({
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
