import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

import {
  HostComponentInfo,
  ContextId,
  ContextIdFactory,
  ContextIdStrategy,
} from '@nestjs/core';
import type { Request } from 'express';

const tenants = new Map<string, ContextId>();

export class AggregateByTenantContextIdStrategy implements ContextIdStrategy {
  attach(contextId: ContextId, request: Request) {
    const tenantId = request.headers['x-tenant-id'] as string;

    let tenantSubTreeId: ContextId;
    if (tenants.has(tenantId)) {
      tenantSubTreeId = tenants.get(tenantId);
    } else {
      tenantSubTreeId = ContextIdFactory.create();
      tenants.set(tenantId, tenantSubTreeId);
    }
    
    return (info: HostComponentInfo) => {
      const context = info.isTreeDurable ? tenantSubTreeId : contextId;
      return context;
    }

    /*// This version works
    return {
      resolve: (info: HostComponentInfo) => {
        const context = info.isTreeDurable ? tenantSubTreeId : contextId;
        return context;
      },
      payload: { tenantId },
    }
    */
  }
}



async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  ContextIdFactory.apply(new AggregateByTenantContextIdStrategy())

  await app.listen(3000);
}
bootstrap();
